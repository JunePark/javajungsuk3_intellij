package ch11.ex;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArrayList01 {
    public static void main(String[] args) {
        String[] strArr = {
                "Hello", "World!"
        };

//        List list = new ArrayList(Arrays.asList(strArr));
        List<String> list = new ArrayList<>(Arrays.asList(strArr));

        System.out.println(list);
    }
}
