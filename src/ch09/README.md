# 소스 파일 목록

## 1. java.lang 패키지 

### 1.1 Object클래스

Object 클래스의 메서드
```
protected Object clone();
public boolean equals(Object obj);
protected void finalize();
public Class getClass();
public int hashCode();
public String toString();
public void notify();
public void notifyAll();
public void wait();
public void wait(long timeout);
public void wait(long timeout, int nanos);
```
* [예제9-1: EqualsEx1.java](EqualsEx1.java)
 
* [예제9-2: EqualsEx2.java](EqualsEx2.java)

* [예제9-3: HashCodeEx1.java](HashCodeEx1.java)

* [예제9-4: CardToString.java](CardToString.java)

* [예제9-5: ToStringTest.java](ToStringTest.java)

* [예제9-6: CardToString2.java](CardToString2.java)

* [예제9-7: CloneEx1.java](CloneEx1.java)
> NOTE. Object의 clone()이 이렇게 가능한 것은 JNI로 구현된 소스의 로직이 원본데이터를 그대로
메모리 카피로 Heap에 생성하는 방식을 사용하기 때문에 primitive 타입의 데이터는 값이 유지가 되고 참조변수는
참조하는 변수값을 그대로 유지하게 된다. 즉, shallow copy가 되는 것이다.

* [예제9-8: CloneEx2.java](CloneEx2.java)

* [예제9-9: ShallowCopy.java](ShallowDeepCopy.java)

* [예제9-10: ClassEx1.java](ClassEx1.java)


### 1.2 String클래스
* [예제9-11: StringEx1.java](StringEx1.java)
> NOTE. String str = "abc" 와 String str = new String("abc") 의 차이점.

* [예제9-12: StringEx2.java](StringEx2.java)

* [예제9-13: StringEx3.java](StringEx3.java)

* [예제9-14: StringEx4.java](StringEx4.java)
> NOTE. StringJoiner

* [예제9-15: StringEx5.java](StringEx5.java)
> NOTE. 문자 인코딩
 
* [예제9-16: StringEx6.java](StringEx6.java)
 
* [예제9-17: StringEx7.java](StringEx7.java)


### 1.3 StringBuffer클래스와 StringBuilder클래스

* [예제9-18: StringBufferEx1.java](StringBufferEx1.java)
> NOTE. equals()는 String과 다르게 override되어 있지 않음. 

* [예제9-19: StringBufferEx2.java](StringBufferEx2.java)


### 1.4 Math클래스
* [예제9-20: MathEx1.java](MathEx1.java)

* [예제9-21: MathEx2.java](MathEx2.java)

* [예제9-22: MathEx3.java](MathEx3.java)


### 1.5 래퍼(wrapper) 클래스
* [예제9-23: WrapperEx1.java](WrapperEx1.java)

* [예제9-24: WrapperEx2.java](WrapperEx2.java)

* [예제9-25: WrapperEx3.java](WrapperEx3.java)




## 2. 유용한 클래스

### 2.1 java.util.Objects클래스
* [예제9-26: ObjectTest.java](ObjectTest.java)


### 2.2 java.util.Random클래스
* [예제9-27: RandomEx1.java](RandomEx1.java)

* [예제9-28: RandomEx2.java](RandomEx2.java)

* [예제9-29: RandomEx3.java](RandomEx3.java)

* [예제9-30: RandomEx4.java](RandomEx4.java)


### 2.3 정규식 - java.util.regex패키지
* [예제9-31: RegularEx1.java](RegularEx1.java)

* [예제9-32: RegularEx2.java](RegularEx2.java)

* [예제9-33: RegularEx3.java](RegularEx3.java)

* [예제9-34: RegularEx4.java](RegularEx4.java)


### 2.4 java.util.Scanner클래스
* [예제9-35: ScannerEx1.java](ScannerEx1.java)

* [예제9-36: ScannerEx2.java](ScannerEx2.java)

* [예제9-37: ScannerEx3.java](ScannerEx3.java)


### 2.5 java.util.StringTokenizer클래스
* [예제9-38: StringTokenizerEx1.java](StringTokenizerEx1.java)

* [예제9-39: StringTokenizerEx2.java](StringTokenizerEx2.java)

* [예제9-40: StringTokenizerEx3.java](StringTokenizerEx3.java)

* [예제9-41: StringTokenizerEx4.java](StringTokenizerEx4.java)
> NOTE. 삼십만삼천백십오 를 303115 변환하는 예제(조금 어렵다.)

* [예제9-42: StringTokenizerEx5.java](StringTokenizerEx5.java)
> NOTE. StringTokenizer는 빈문자열을 토큰으로 인식하지 않는다.


### 2.6 java.math.BigInteger클래스
* [예제9-43: BigIntegerEx.java](BigIntegerEx.java)


### 2.6 java.math.BigDecimalEx클래스
* [예제9-44: BigDecimalEx.java](BigDecimalEx.java)
