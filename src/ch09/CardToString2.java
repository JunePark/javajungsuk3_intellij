package ch09;

class Card_2 {
    String kind;
    int number;

    Card_2() {
        this("SPADE", 1);  // Card(String kind, int number)를 호출
    }

    Card_2(String kind, int number) {
        this.kind = kind;
        this.number = number;
    }

    public String toString() {
        // Card인스턴스의 kind와 number를 문자열로 반환한다.
        return "kind : " + kind + ", number : " + number;
    }
}

class CardToString2 {
    public static void main(String[] args) {
        Card_2 c1 = new Card_2();
        Card_2 c2 = new Card_2("HEART", 10);
        System.out.println(c1.toString());
        System.out.println(c2.toString());

        System.out.println(c1);
        System.out.println(c2);
    }
}
