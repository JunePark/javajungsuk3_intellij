package ch09;

import java.util.*;

class Circle_1 implements Cloneable {
    Point_1 p;  // 원점
    double r; // 반지름

    Circle_1(Point_1 p, double r) {
        this.p = p;
        this.r = r;
    }

    public Circle_1 shallowCopy() { // 얕은 복사
        Object obj = null;

        try {
            obj = super.clone();
        } catch(CloneNotSupportedException e) {
        }

        return (Circle_1) obj;
    }

    public Circle_1 deepCopy() { // 깊은 복사
        Object obj = null;

        try {
            obj = super.clone();
        } catch(CloneNotSupportedException e) {
        }

        Circle_1 c = (Circle_1) obj;
        c.p = new Point_1(this.p.x, this.p.y);

        return c;
    }

    public String toString() {
        return "[p=" + p + ", r=" + r + "]";
    }
}

class Point_1 {
    int x;
    int y;

    Point_1(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public String toString() {
        return "(" + x + ", " + y + ")";
    }
}

class ShallowDeepCopy {
    public static void main(String[] args) {
        Circle_1 c1 = new Circle_1(new Point_1(1, 1), 2.0);
        Circle_1 c2 = c1.shallowCopy();
        Circle_1 c3 = c1.deepCopy();

        System.out.println("c1=" + c1);
        System.out.println("c2=" + c2);
        System.out.println("c3=" + c3);
        c1.p.x = 9;
        c1.p.y = 9;
        System.out.println("= c1의 변경 후 =");
        System.out.println("c1=" + c1);
        System.out.println("c2=" + c2);
        System.out.println("c3=" + c3);
    }
}
