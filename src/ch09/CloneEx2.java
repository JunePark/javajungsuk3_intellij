package ch09;

import java.util.*;

class CloneEx2 {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};

        // 배열 arr을 복제해서 같은 내용의 새로운 배열을 만든다.
        int[] arrClone = arr.clone();
        arrClone[0] = 6;

        System.out.println(Arrays.toString(arr));
        System.out.println(Arrays.toString(arrClone));

        System.out.println();
        System.out.println(arr.getClass());
        System.out.println(arrClone.getClass());

        System.out.println();
        System.out.println(arr.getClass().getName());
        System.out.println(arr.getClass().toGenericString());
        System.out.println(arr.getClass().toString());

        char[] arr2 = {'H', 'W'};
        System.out.println();
        System.out.println(arr2.getClass().getName());
        System.out.println(arr2.getClass().toGenericString());
        System.out.println(arr2.getClass().toString());

        String[] arr3 = {"Hello", "World!"};
        System.out.println();
        System.out.println(arr3.getClass().getName());
        System.out.println(arr3.getClass().toGenericString());
        System.out.println(arr3.getClass().toString());
    }
}
