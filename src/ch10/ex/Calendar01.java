package ch10.ex;

import java.util.Calendar;
import java.util.Date;

public class Calendar01 {
    public static void main(String[] args) {
        Calendar cal = Calendar.getInstance();
        Date d = new Date(cal.getTimeInMillis());
        System.out.println("Calendar를 Date로 변환");
        System.out.println(cal);
        System.out.println(d);

        Date d1 = new Date();
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(d1);
        System.out.println("Date를 Calendar로 변환");
        System.out.println(cal1);
        System.out.println(d1);
    }
}
