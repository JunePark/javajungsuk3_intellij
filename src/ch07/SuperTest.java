package ch07;

class SuperTest {
    public static void main(String args[]) {
        Child_1 c = new Child_1();
        c.method();
    }
}

class Parent_1 {
    int x = 10;
}

class Child_1 extends Parent_1 {
    void method() {
        System.out.println("x=" + x);
        System.out.println("this.x=" + this.x);
        System.out.println("super.x=" + super.x);
    }
}
