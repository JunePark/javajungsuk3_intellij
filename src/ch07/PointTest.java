package ch07;

class PointTest {
    public static void main(String args[]) {
        Point3D1 p3 = new Point3D1(1, 2, 3);
    }
}

class Point1 {
    int x;
    int y;

    Point1(int x, int y) {
        this.x = x;
        this.y = y;
    }

    String getLocation() {
        return "x :" + x + ", y :" + y;
    }
}

class Point3D1 extends Point1 {
    int z;

    Point3D1(int x, int y, int z) {
// 에러발생
/*
 required: int,int
  found: no arguments
  reason: actual and formal argument lists differ in length

        this.x = x;
        this.y = y;
        this.z = z;
*/
        super(x,y);
        this.z = z;
    }

    String getLocation() {    // 오버라이딩
        return "x :" + x + ", y :" + y + ", z :" + z;
    }
}
