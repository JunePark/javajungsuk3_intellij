package ch06;

class Data3 {
    int x;
}

class PrimitiveParamEx {
    public static void main(String[] args) {
        Data3 d = new Data3();
        d.x = 10;
        System.out.println("main() : x = " + d.x);

        change(d.x);
        System.out.println("After change(d.x)");
        System.out.println("main() : x = " + d.x);
    }

    static void change(int x) {  // 기본형 매개변수
        x = 1000;
        System.out.println("change() : x = " + x);
    }
}
