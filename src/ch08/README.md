# 소스 파일 목록

```
용어를 정리해보자.

프로그램 오류(program fault)
- 컴파일 오류 : 컴파일 시점에 발견
- 런타임 오류 : 실행 시점에 발견 <= 이부분이 처리 대상
- 논리적 오류 : 실행 시점에 발견

자바는 런타임 오류를 다시 에러(Error) 와 예외(Exception) 으로 분류.
- Error : 수습이 불가능한 오류
- Exception : 프로그래머가 작성한 코드에 의해 수습가능한 오류

즉, 예외처리란 Error와 Exception을 처리하는 것을 모두 포함하지만 주로 Exception 처리에 집중.

컴파일러가 런타임 오류를 확인하는지 여부에 따라
- unchecked 예외 : RuntimeException 클래스 포함 하위 클래스. 그리고 Error까지 포함한 클래스
- checked 예외 : 그 이외의 클래스
(예외라고 해놓고 Error까지 포함하는 용어가 그리 매끄럽지 못하다.
 이는 예외에 관련된 용어가 대부분 이런식으로 불분명하게 정의하고 있기 때문이다.
 가끔 문맥에 따라 이와 같이 Error를 포함한 것을 예외로 할때도 있다.
 )
```

## 1. 예외처리(exception handling)

### 1.1 프로그램 오류

### 1.2 예외 클래스의 계층구조

### 1.3 예외처리하기 try-catch 문
* [예제8-1: ExceptionEx1.java](ExceptionEx1.java)
 
* [예제8-2: ExceptionEx1.java](ExceptionEx2.java)
> NOTE. 실수를 zero로 나누어도 예외가 발생하지 않음(결과값: Infinity)

* [예제8-3: ExceptionEx3.java](ExceptionEx3.java)

### 1.4 try-catch문에서의 흐름
* [예제8-4: ExceptionEx4.java](ExceptionEx4.java)
 
* [예제8-5: ExceptionEx5.java](ExceptionEx5.java)

### 1.5 예외의 발생과 catch블럭
* [예제8-6: ExceptionEx6.java](ExceptionEx6.java)

* [예제8-7: ExceptionEx7.java](ExceptionEx7.java)
> NOTE. printStackTrace(), getMessage()

* [예제8-8: ExceptionEx8.java](ExceptionEx8.java)
> NOTE. 멀티 catch문은 | 기호로 여러개의 catch문을 하나로 묶음.
```
catch(ArithmeticException | IndexOutOfBoundsException e) {

}
```

### 1.6 예외 발생시키기
* [예제8-9: ExceptionEx9.java](ExceptionEx9.java)
 
* [예제8-10: ExceptionEx10.java](ExceptionEx10.java)
 
* [예제8-11: ExceptionEx11.java](ExceptionEx11.java)


### 1.7 메서드에 예외 선언하기
* [예제8-12: ExceptionEx12.java](ExceptionEx12.java)

* [예제8-13: ExceptionEx13.java](ExceptionEx13.java)

* [예제8-14: ExceptionEx14.java](ExceptionEx14.java)

* [예제8-15: ExceptionEx15.java](ExceptionEx15.java)
> NOTE. IntelliJ에서 실행시 argument를 주는 방법이 번거로운데, powershell에서 공백인자는 다음과 같은 방식으로 하면 된다.
```
terminal을 실행한 다음 (powershell이 terminal로 설정된 경우)
> cd path_to\out\production\javajungsuk3_intellij
> java -cp . ch08.ExceptionEx15 `"`"
```

* [예제8-16: ExceptionEx16.java](ExceptionEx16.java)


### 1.8 finally블럭
* [예제8-17: FinallyTest.java](FinallyTest.java)

* [예제8-18: FinallyTest2.java](FinallyTest2.java)

* [예제8-19: FinallyTest3.java](FinallyTest3.java)


### 1.9 자동 자원 변환 try-with-resources문
* [예제8-20: TryWithResourceEx.java](TryWithResourceEx.java)


### 1.10 사용자정의 예외 만들기
* [예제8-21: NewExceptionTest.java](NewExceptionTest.java)


### 1.11 예외 되던지기
* [예제8-22: ExceptionEx17.java](ExceptionEx17.java)


### 1.12 연결된 예외
* [예제8-23: ChainedExceptionEx.java](ChainedExceptionEx.java)
