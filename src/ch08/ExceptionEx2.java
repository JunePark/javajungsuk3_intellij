package ch08;

class ExceptionEx2 {
    public static void main(String args[]) {
        int number = 100;
        int result = 0;

        double d = 100.;
        System.out.println( d / 0); // 실수를 zero로 나누기(예외 발생안함) : 결과값 Infinity

        for(int i = 0; i < 10; i++) {
            result = number / (int) (Math.random() * 10); // 12번째 라인
            System.out.println(result);
        }

    } // main의 끝
}
