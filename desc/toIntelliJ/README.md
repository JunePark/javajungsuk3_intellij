#

##

* 프로젝트 생성
![IntelliJ](./images/IntelliJ-0000.png)

![IntelliJ](./images/IntelliJ-0001.png)

![IntelliJ](./images/IntelliJ-0002.png)

![IntelliJ](./images/IntelliJ-0003.png)

![IntelliJ](./images/IntelliJ-0004.png)

* 소스를 복사(여기서는 ch10, ch11을 복소)
![IntelliJ](./images/IntelliJ-0005.png)

* ch10에 있는 소스들을 모두 선택한 다음 우측 클릭하여 Reformat Code
![IntelliJ](./images/IntelliJ-0006.png)

* 소스 코드별로 패키지명 할당
![IntelliJ](./images/IntelliJ-0007.png)

![IntelliJ](./images/IntelliJ-0008.png)

* 실행
![IntelliJ](./images/IntelliJ-0009.png)

* 실행 (소스코드 우측 마우스 클릭)
![IntelliJ](./images/IntelliJ-0010.png)

* 소스코드를 UTF-8 로 변경
![IntelliJ](./images/IntelliJ-0011.png)

![IntelliJ](./images/IntelliJ-0012.png)

![IntelliJ](./images/IntelliJ-0013.png)

![IntelliJ](./images/IntelliJ-0014.png)

* 개별로 소스 컴파일 할 경우
```
javac -d . source.java -encoding UTF-8
```
> NOTE. command창에서 컴파일 하는 경우